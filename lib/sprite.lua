local class = require("lib.class")
local Vec2 = require("lib.vector2")
local Anim = require("lib.animations")

local Sprite = class:derive("sprite")

function Sprite:new(atlas,w,h,x,y,sx,sy,angle)
    self.w = w
    self.h = h
    self.flip = Vec2(1,1)
    self.pos = Vec2(x or 0 ,y or 0)
    self.scale = Vec2(sx or 1,sy or 1)
    self.atlas = atlas
    self.angle = angle or 0
    self.animations = {}
    self.current_anim = ""
    self.quad = love.graphics.newQuad(0,0,w,h,atlas:getDimensions())
end

function Sprite:animation(anim_name)
    if(self.current_anim ~= anim_name and self.animations[anim_name] ~= nil) then
        self.current_anim = anim_name
        self.animations[anim_name]:reset()
        self.animations[anim_name]:set_quad(self.quad)
    end
end

function Sprite:end_animation()
    if self.animations[self.current_anim] ~= nil then
        return self.animations[self.current_anim].done 
    end
    return true
end

function Sprite:flip_h(flip)
    if flip then
        self.flip.x = -1
    else
        self.flip.x = 1
    end
end

function Sprite:add_animation(name,anim)
    self.animations[name] = anim
end

function Sprite:add_animations(animations)
    assert(type(animations) == "table" , "animations parameter")
    for k,v in pairs(animations) do
        self.animations[k] = v
    end
end

function Sprite:update(dt)
    if(self.animations[self.current_anim] ~= nil ) then
        self.animations[self.current_anim]:update(dt,self.quad)
    end
end

function Sprite:draw()
    love.graphics.draw(self.atlas,self.quad,self.pos.x,self.pos.y,self.angle,self.scale.x * self.flip.x ,self.scale.y,self.w /2,self.h /2)
end

return Sprite