--libs
local SM     = require("lib.state_machine")
local Anim   = require("lib.animations")
local Sprite = require("lib.sprite")

local Player = SM:derive("player")

--Data
local fox
local sound

--Animations
local fox_idle = Anim(0,172,150,86,3,3,5)
local fox_run = Anim(0,0,150,86,8,8,16)
local fox_walk = Anim(0,86,150,86,8,8,12)

function Player:new(state)
    if fox == nil then
        fox = love.graphics.newImage("Sprite/FoxRun.png")
    end
    if sound == nil then
        sound = love.audio.newSource("Sound/step.wav","stream")
    end
    --fox_sprite = love.graphics.newQuad(0,0,150,86,fox:getDimensions())
    self.fox_sprite = Sprite(fox,150,86,80,450)
    --[[fox_sprite:add_animation("idle",fox_idle)
    fox_sprite:add_animation("walk",fox_walk)
    fox_sprite:add_animation("run",fox_run)]]--
    self.fox_sprite:add_animations({idle=fox_idle,walk=fox_walk,run=fox_run})
    self.fox_sprite:animation("idle")
    self.vx = 0

    self.super.new(self,state)
end

--player_states
function Player:idle_enter(dt)
    self.fox_sprite:animation("idle")
end

function Player:idle(dt)
    if Key:key("left") or Key:key("right") then
        self:change("walk")
    elseif Key:key("space") then
        self:change("idle")
    end
end

function Player:walk_enter(dt)
    self.fox_sprite:animation("walk")
end

function Player:walk(dt)
    if Key:key("right") and not Key:key("left") and vx ~= 1 then
        self.fox_sprite:flip_h(false)
        self.vx = 1
    elseif Key:key("left") and not Key:key("right") and vx ~= -1 then
        self.fox_sprite:flip_h(true)
        self.vx = -1
    elseif not Key:key("left") and not Key:key("right") then
        self.vx = 0
        self:change("idle")
    end

    if  self.fox_sprite.current_anim == "walk" and Key:key_down("z") then
        self:change("run")
    end
end

function Player:run_enter(dt)
    self.fox_sprite:animation("run")
end

function Player:run(dt)
    if Key:key("right") and not Key:key("left") and vx ~= 1 then
        self.fox_sprite:flip_h(false)
        self.vx = 1
    elseif Key:key("left") and not Key:key("right") and vx ~= -1 then
        self.fox_sprite:flip_h(true)
        self.vx = -1
    elseif not Key:key("left") and not Key:key("right") then
        self.vx = 0
        self:change("idle")
    end
    love.audio.play(sound)
end

function Player:update(dt)
    self.super.update(self,dt)
    self.fox_sprite:update(dt)
    if(self.fox_sprite.current_anim=="walk") then
        self.fox_sprite.pos.x = self.fox_sprite.pos.x + self.vx *100 *dt
    end
    if(self.fox_sprite.current_anim=="run") then
        self.fox_sprite.pos.x = self.fox_sprite.pos.x + self.vx *200 *dt
    end
end

function Player:draw()
    self.fox_sprite:draw()
end

return Player