--libs
local Scene  = require("lib.scene")
local Player   = require("../player")

--Scene
local T = Scene:derive("level_1")

local fox_sprite

function T:new(scene_mngr)
    self.super.new(self,scene_mngr)
end

local entered = false

function T:enter()
    T.super.enter(self)
    if not entered then
        entered = true
        self.p = Player("idle")
        self.em:add(self.p)
    end
end

function T:update(dt)
    self.super.update(self,dt)
end

function T:draw()
    love.graphics.clear(0.34,0.38,1)
    self.super.draw(self)
    love.graphics.print("move with the directional keys (left / right)",960/2-50,20)
    love.graphics.print("run pressing 'Z' while you walk",960/2-50,40)
    love.graphics.print("back to the main menu (press 'M')",960/2-50,60)
end

return T
