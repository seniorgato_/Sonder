--libs
local SM    = require("lib.scene_mngr")
local Event = require("lib.event")
Key = require("lib.keyboard")
local sm

function love.load()
	love.graphics.setDefaultFilter('nearest','nearest')
	local font = love.graphics.newFont("Font/babyblue.ttf",16)
	love.graphics.setFont(font)

	_G.events = Event(false)
	Key:hook_love_events()

	sm = SM("Scenes",{"main_menu","level_1"})
	sm:switch("main_menu")
end

function love.update(dt)
	if dt>0.035 then return end
	
	if Key:key_down("n") then
		sm:switch("level_1")
	end
	if Key:key_down("m") then
		sm:switch("main_menu")
	end
	if Key:key_down("escape") then
		love.event.quit()
	end
	
	sm:update(dt)
	Key:update(dt)
end

function love.draw()
	sm:draw()
end


